# Faircorp Android App

- Author : Alexandre Verdet
- Android App of the 2020 Web & Mobile Programing project for [EMSE's Computer Science Major](https://ci.mines-stetienne.fr/m-info/wmp/)
- Based on [Guillaume Ehret's course](https://dev-mind.fr/formations.html)

## Context

The goal of the 2020 Web & Mobile Programing project is to implement a fully functionnal Building management system (especially rooms, windows and heaters controllers).\
The project is composed of a [Java SpringBoot Backend server](https://gitlab.com/averd/spring-practice), an [Android Kotlin Application](https://gitlab.com/averd/faircorp-android-app) and a [VueJS Frontend server](https://gitlab.com/averd/faircorp-vue-app) interacting together. An extension of this project can be made with Arduinos Controlers in the [IoT course](https://ci.mines-stetienne.fr/m-info/iot/), in order to actually control devices.

## Project setup

Using [Android Studio](https://developer.android.com/studio), you can build and run the app on your smartphone or on a virtual device.
